package zielware.com;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

import javax.swing.JPanel;

public class BallComponent extends JPanel {

	private static final long serialVersionUID = -6420616017630762259L;
	
	private ArrayList<Ball> balls = new ArrayList<Ball>();
	
	public void addBallToComponent(Ball ball) {
		balls.add(ball);
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2D = (Graphics2D) g;
		for(Ball b : balls) {
			g2D.fill(b.getShape());
		}
	}
}
