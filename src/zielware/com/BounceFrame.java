package zielware.com;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class BounceFrame extends JFrame {
	
	private static final long serialVersionUID = 3403056090633789194L;	
	private BallComponent comp;
	public static final int DEFAULT_WIDTH = 450;
	public static final int DEFAULT_HEIGHT = 350;
	public static final int STEPS = 1000;
	public static final int DELAY = 3;
	
	BounceFrame(){
		setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
		setTitle("Pilka");
		
		comp = new BallComponent();
		add(comp, BorderLayout.CENTER);
		JPanel buttonPanel = new JPanel();

		addButton(buttonPanel, "Start", e -> addBall());
		addButton(buttonPanel, "Zamknij", e -> System.exit(0));

		add(buttonPanel, BorderLayout.SOUTH);		
	}
	
	public void addButton(Container c, String tittle, ActionListener listener) {
		JButton button = new JButton(tittle);
		c.add(button);
		button.addActionListener(listener);
	}
	
	public void addBall() {
		Ball ball = new Ball();
		comp.addBallToComponent(ball);
		Runnable r = new BallRunnable(ball, comp);
		Thread t = new Thread(r);
		t.start();
	}
}
