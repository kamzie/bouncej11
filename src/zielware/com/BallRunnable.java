package zielware.com;

import java.awt.Component;

public class BallRunnable implements Runnable {

	private Ball ball;
	private Component component;
	public static final int STEPS = 1000;
	public static final int DELAY = 5;
	
	public BallRunnable(Ball b, Component c) {
		ball = b;
		component = c;
	}

	@Override
	public void run() {
		for(int i = 1 ; i <= STEPS; i ++) {
			ball.moveBallAndCalculateDirection(component.getBounds());
			component.repaint();
			try {
				Thread.sleep(DELAY);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}		
	}
}
